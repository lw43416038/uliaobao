# -*- coding: utf-8 -*-
import scrapy
from scrapy_redis.spiders import RedisSpider
import re
from ..items import TotalItem, MianLiaoItem
from datetime import date
import json
import math


class UlbSpider(RedisSpider):
    name = 'ulb'
    allowed_domains = ['uliaobao.com']
    # start_urls = ['http://uliaobao.com/']
    redis_key = 'ulb'

    def make_requests_from_url(self, url):
        # if 'skipTendencyList' in url:
        #     return scrapy.Request(url, callback=self.parse_ml)
        # else:
        #     return scrapy.Request(url, callback=self.parse_total)
        return scrapy.Request(url, callback=self.parse_total)

    # def parse_ml(self, response):
    #     payloadHeader = {'Content-Type': 'application/json'}
    #     payloadData = '{"categoryId":"","suitableStyleIds":[],"seasonIds":[],"styleIds":[],"size":10,"pageNum":1}'
    #     postUrl = 'http://www.uliaobao.com/s//tendencyList'
    #     yield scrapy.Request(postUrl, method='POST', headers=payloadHeader, body=payloadData,
    #                          callback=self.parse_ml_list, dont_filter=True)
    #
    # def parse_ml_list(self, response):
    #     start_data = json.loads(response.body.decode())
    #     # print(start_data)
    #     totalNum = start_data['count']
    #     totalPage = math.ceil(totalNum / 10)
    #     for i in range(1, totalPage + 1):
    #         payloadHeader = {'Content-Type': 'application/json'}
    #         payloadData = {"categoryId": "", "suitableStyleIds": [], "seasonIds": [], "styleIds": [], "size": 10,
    #                        "pageNum": i}
    #         payloadData = json.dumps(payloadData)
    #         postUrl = 'http://www.uliaobao.com/s//tendencyList'
    #         yield scrapy.Request(postUrl, method='POST', headers=payloadHeader, body=payloadData,
    #                              callback=self.parse_ml_creat)
    #
    # def parse_ml_creat(self, response):
    #     data = json.loads(response.body.decode())
    #     for i in data['data']:
    #         trendId = i['trendId']
    #         url = 'http://www.uliaobao.com/s/particular?trendId={}'.format(trendId)
    #         yield scrapy.Request(url, callback=self.parse_ml_detail)
    #
    # def parse_ml_detail(self, response):
    #     item = MianLiaoItem()
    #     item['mainUrl'] = response.xpath("//div[@class='bannerMain']/img/@src").extract_first()
    #     item['mainTitle'] = response.xpath("//div[@class='bannerMain']//h3/text()").extract_first()
    #     item['mainDesc'] = response.xpath("//div[@class='bannerMain']//p/text()").extract_first()
    #     item['secondData'] = []
    #     item['referer'] = response.url
    #     for i in response.xpath("//div[@class='bigDetailTpBox']/div"):
    #         data_dict = {}
    #         data_dict['title'] = i.xpath("./div[@class='TpHeader']/h4/text()").extract_first()
    #         data_dict['detailInfo'] = []
    #         for j in i.xpath("./div[@class='3TpMiddle']//li"):
    #             data_dict['detailInfo'].append({'modelImgUrl': j.xpath("./a/img/@src").extract_first(),
    #                                             'minaliaoUrl': j.xpath("./div/a/img/@src").extract_first(),
    #                                             'mianliaoName': j.xpath("./div/p/text()").extract_first()})
    #         data_dict['secondDesc'] = i.xpath("./div[@class='3TpMiddle']/p/text()").extract_first()
    #         item['secondData'].append(data_dict)
    #     yield item

    def parse_total(self, response):
        a_li = response.xpath("//div[@id='Content_left_menu']/div[position()<9]/div/a")
        limit_li = response.xpath("//div[@id='item-9']/div/a/@href").extract()
        limit_li = [i.replace("/s/fabrics?page=1", "") for i in limit_li]
        for i in a_li:
            url = i.xpath("./@href").extract_first()
            url = response.urljoin(url)
            yield scrapy.Request(url, callback=self.creat_limit, dont_filter=True, meta={"limit": limit_li})

    def creat_limit(self, response):
        goodsNum = int(response.xpath("//div[@class='navRight']/p/span/text()").extract_first().strip())  # 每个分类的商品总数
        if goodsNum > 1000:
            limit_li = response.meta["limit"]
            for i in limit_li:
                url = response.url + i + '&flag=2'
                yield scrapy.Request(url, callback=self.creat_limit2)

        else:
            num = response.xpath("//a[@class='nextBtn']/preceding-sibling::i[1]/text()").extract_first()
            url = response.url
            for i in range(1, int(num) + 1):
                url = re.sub(r'page=\d+', 'page={}'.format(i), url)
                yield scrapy.Request(url, callback=self.parse_list)

    def creat_limit2(self, response):
        goodsNum = int(response.xpath("//div[@class='navRight']/p/span/text()").extract_first().strip())
        if goodsNum > 1000:
            startPrice = response.meta.get('start')
            if not startPrice:
                startPrice = 0
            endPrice = response.meta.get('end')
            if not endPrice:
                endPrice = float(response.xpath("//ul[@id='picId']/li[1]/div/span/span/text()").extract_first())
            midPrice = (endPrice - startPrice) / 2 + startPrice
            midPrice = round(midPrice, 2)
            if endPrice - startPrice < 0.1:
                self.logger.error("特殊情况:" + response.url)
            else:

                url = re.sub('&startPrice=.*?&endPrice=.*', '', response.url)
                url1 = url + "&startPrice={}&endPrice={}".format(startPrice, midPrice)
                url2 = url + "&startPrice={}&endPrice={}".format(midPrice, endPrice)
                yield scrapy.Request(url1, callback=self.creat_limit2, meta={"start": startPrice, "end": midPrice})
                yield scrapy.Request(url2, callback=self.creat_limit2, meta={"start": midPrice, "end": endPrice})

        else:
            num = response.xpath("//a[@class='nextBtn']/preceding-sibling::i[1]/text()").extract_first()
            url = response.url
            for i in range(1, int(num) + 1):
                url = re.sub(r'page=\d+', 'page={}'.format(i), url)
                yield scrapy.Request(url, callback=self.parse_list)

    def parse_list(self, response):
        url_li = response.xpath("//ul[@id='picId']/li/a/@href").extract()
        for url in url_li:
            url = response.urljoin(url)
            yield scrapy.Request(url, callback=self.parse_detail)

    def parse_detail(self, response):
        item = TotalItem()
        item['bClassify'] = response.xpath("//div[@class='navLeft clearfix']/ul/li[3]//text()").extract_first()
        item['sClassify'] = response.xpath("//div[@class='navLeft clearfix']/ul/li[4]//text()").extract_first()
        item['goodsName'] = response.xpath("//div[@class='navLeft clearfix']/ul/li[5]//text()").extract_first()
        ysprice = response.xpath("//div[@class='backgd']/div[2]/div[1]/script").re('changePrice\((.*?)\)')[-2]
        ysprice = ysprice.replace("'", "").split(',')[:-1]
        item['ysprice'] = '/'.join(ysprice)
        dhprice = response.xpath("/html/body/script[9]").re('samplePrice:"(.*?)"')[0]
        unit = response.xpath("/html/body/script[9]").re('samplePriceUnitName:"(.*?)"')[0]
        item['dhprice'] = dhprice + '/' + unit
        item['goodsNum'] = response.xpath("//div[@class='product-num']/span/text()").extract_first().strip()
        item['corPattern_src'] = response.xpath(
            "//div[@class='sku-all']/ul/li[@class='js-can-choosen']/a/img/@src").extract_first()
        item['corPattern_src'] = re.sub(r'\?.*', '', item['corPattern_src']) if item['corPattern_src'] else None
        item['corPattern_name'] = response.xpath(
            "//div[@class='sku-all']/ul/li[@class='js-can-choosen']/i/text()").extract_first()
        shelf_time = re.search('/\d+/\d+/', item['corPattern_src']).group().split('/') if item[
            'corPattern_src'] else None
        try:
            year, month, day = int(shelf_time[1]), int(int(shelf_time[2][:2])), int(int(shelf_time[2][2:]))
        except:
            self.logger.info('图片加载有误')
        else:
            item['shelf_time'] = date(year, month, day)
        deliver_time = response.xpath("//div[@class='shippInfor clearfix']/p[1]/text()").extract_first()
        item['deliver_time'] = deliver_time.replace('发货时间：', '').strip() if deliver_time else None
        freight = response.xpath("//div[@class='shippInfor clearfix']/p[2]/text()").extract_first()
        item['freight'] = freight.replace('运费：', '').strip() if deliver_time else None
        item['prodPlace'] = response.xpath("//div[@class='shippInfor clearfix']/p[3]/text()").extract_first()
        item['prodPlace'] = item['prodPlace'].replace('产地：', '')
        style = response.xpath("//li[contains(text(),'适用款式：')]/text()").extract_first()
        item['style'] = style.replace('适用款式：', '').strip() if style else None
        crowd = response.xpath("//li[contains(text(),'适用人群：')]/text()").extract_first()
        item['crowd'] = crowd.replace('适用人群：', '').strip() if crowd else None
        season = response.xpath("//li[contains(text(),'季节：')]/text()").extract_first()
        item['season'] = season.replace('季节：', '').strip() if season else None
        classify = response.xpath("//li[contains(text(),'分类：')]/text()").extract_first()
        item['classify'] = classify.replace('分类：', '').strip() if classify else None
        goodsDesc = response.xpath("//div[@class='fabric-detailBox']/div//img/@src").extract()
        goodsDesc = [re.sub(r'\?.*', '', i) for i in goodsDesc] if goodsDesc else None
        item['goodsDesc'] = goodsDesc
        item['sense_img'] = response.xpath("//div[@class='leftSen fl']/img[@id='imgs']/@src").extract_first()
        item['sense_img'] = re.sub(r'\?.*', '', item['sense_img']) if item['sense_img'] else None
        item['sense_thickness'] = response.xpath(
            "//div[@class='rightSen fl']/p[contains(text(),'厚')]/span/em[@class='connection']/text()").extract_first()
        item['sense_elasticity'] = response.xpath(
            "//div[@class='rightSen fl']/p[contains(text(),'弹')]/span/em[@class='connection']/text()").extract_first()
        item['sense_compliance'] = response.xpath(
            "//div[@class='rightSen fl']/p[contains(text(),'柔软度')]/span/em[@class='connection']/text()").extract_first()
        item['sense_permeability'] = response.xpath(
            "//div[@class='rightSen fl']/p[contains(text(),'透气度')]/span/em[@class='connection']/text()").extract_first()
        shopData = response.xpath("//h3[@class='js-person-info']/text()").extract_first()
        shopData = re.sub('\s+', '', shopData)
        item['shopkeeper'] = re.search('[\u4e00-\u9fa5]+', shopData)
        item['shopkeeper'] = item['shopkeeper'].group() if item['shopkeeper'] else None
        item['phone'] = re.search('\d+', shopData).group()
        item['shopName'] = response.xpath("//a[@title='查看店铺详情']/p/text()").extract_first()
        item['scope'] = response.xpath("//div[@class='mainType']/span/text()").extract_first()
        sameType = response.xpath("//h2[@id='tab1']/following-sibling::div[1]/div/a")
        sameType_list = []
        for i in sameType:
            sameType_data = {}
            sameType_data['url'] = response.urljoin(i.xpath("./@href").extract_first())
            sameType_data['goodsName'] = i.xpath("./@title").extract_first()
            sameType_data['price'] = i.xpath(".//span[@itemprop='price']/text()").extract_first()
            sameType_list.append(sameType_data)
        item['sameType'] = sameType_list
        yield item
