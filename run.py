import os
from redis import Redis
from multiprocessing import Process
from uliaobao.settings import REDIS_HOST, REDIS_PORT, REDIS_PARAMS

def run():
    os.system('scrapy crawl ulb')


if __name__ == '__main__':
    r = Redis(host=REDIS_HOST, port=REDIS_PORT, password=REDIS_PARAMS)
    r.rpush('ulb', 'http://www.uliaobao.com/')
    p_list = []
    for i in range(4):
        p = Process(target=run)
        p.start()
        p_list.append(p)
    for j in p_list:
        j.join()
